#include "AgenteAestrella.h"
#include <math.h>

using namespace std;

AgenteAestrella::AgenteAestrella(){
	this->open_list = new linked_list;
    this->closed_list = new linked_list;
}

AgenteAestrella::AgenteAestrella(bool tipo_agente){
    this->open_list = new linked_list;
    this->closed_list = new linked_list;
    this->tipo_agente = tipo_agente;
}

void AgenteAestrella::Resolve(Matriz *matrix, int X, int Y){
	//aqui hace toda la "magia" con la lista y demas para 
	//calcular el camino
	this->cantidad_celdas = 0;
    //guardamos la celda meta
    this->meta = matrix->GetMeta();

	//obtenemos la celda inicial y la añadimos a la lista
	this->open_list->append(matrix->GetCelda(X,Y));
    this->open_list->get_last_node()->set_h(H(X,Y,this->meta->getX(),this->meta->getY(),this->tipo_agente));
    this->open_list->get_last_node()->set_g(0);

    //ejecutamos el bucle hasta que la lista abierta este vacia
    while(open_list->get_size() > 0){
        //chapuza, pero son las 2 de la mañana y esto no funciona,asi que no me queda otra
        //recorremos la lista buscando el valor de g+h mas pequeño
        int min_value = -1;
        int final_value = -1;
        for(int i = 0;i<this->open_list->get_size();i++){
            if(min_value == -1){
                min_value = this->open_list->get_at(i)->get_f();
                final_value = i;
            } else if(min_value > this->open_list->get_at(i)->get_f()){
                min_value = this->open_list->get_at(i)->get_f();
                final_value = i;
            }
        }

        this->closed_list->append(this->open_list->remove_at(final_value));
        this->current = this->closed_list->get_last();
        this->parentnode = this->closed_list->get_last_node();
        
        //si este nodo es el objetivo, hemos acabado
        if(this->current == this->meta){
            break;
        }        

        //obtenemos vecinos (0=arriba, 1=Izquierda, 2=Derecha, 3=abajo)
        this->vecinos[0] = matrix->GetCelda(this->current->getX(),this->current->getY()-1);
        this->vecinos[1] = matrix->GetCelda(this->current->getX()-1,this->current->getY());
        this->vecinos[2] = matrix->GetCelda(this->current->getX()+1,this->current->getY());
        this->vecinos[3] = matrix->GetCelda(this->current->getX(),this->current->getY()+1);
        for(int j=0;j<4;j++){
            //comprobamos si es null o debemos ignorarlo o si ya lo hemos visitado o es un obstaculo
            if((this->vecinos[j] != nullptr) && !this->vecinos[j]->isObstaculo()){
                //creamos un nodo nuevo y establecemos sus pesos y su padre
                this->nodeptr = new Node(this->vecinos[j]);
                this->cantidad_celdas++;
                this->nodeptr->set_parent(this->parentnode);
                this->nodeptr->set_g(this->parentnode->get_g()+1);
                this->nodeptr->set_h(H(this->vecinos[j]->getX(),this->vecinos[j]->getY(),this->meta->getX(),this->meta->getY(),this->tipo_agente));
                //comprobamos si el nodo esta en la lista open, en caso afirmativo devuelve el nodo, en caso negativo nullptr
                this->tempnode = this->open_list->in_path(this->vecinos[j]);
                if(this->tempnode != nullptr){
                    //si el que esta en la lista es mejor, lo descartamos
                    if(this->nodeptr->get_f() >= this->tempnode->get_f()){
                        delete this->nodeptr;
                        continue;
                    }
                }
                //comprobamos si el nodo esta en la lista closed, en caso afirmativo devuelve el nodo, en caso negativo nullptr
                this->tempnode = this->closed_list->in_path(this->vecinos[j]);
                if(this->tempnode != nullptr){
                    //si el esta en la lista es mejor, lo descartamos
                    if(this->nodeptr->get_f() >= this->tempnode->get_f()){
                        delete this->nodeptr;
                        continue;
                    }
                } 
                //eliminamos el nodo de la lista closed
                this->closed_list->remove_node(this->tempnode);
                //eliminamos el nodo de la lista open
                this->tempnode = this->open_list->in_path(this->vecinos[j]);
                this->open_list->remove_node(this->tempnode);
                //añadimos el nuevo nodo a la lista open
                this->open_list->append(this->nodeptr);
            }
        }
        //ordenamos segun el peso, el nodo de menor peso estara en al principio
        //this->open_list->sort();
    }

    //hemos salido del while, por lo que hemos encontrado el camino, ahora hay que crear una lista con los pasos
    //usamos final_list
    if(this->current == this->meta){
        this->final_list = new linked_list;
        while(this->parentnode != nullptr){
            //usamos final_list
            final_list->append(this->parentnode);
            this->parentnode = this->parentnode->get_parent();
        }
        //eliminamos el ultimo elemento añadido porque sobra
        this->final_list->pop_last();
    }else {
        this->final_list = nullptr;
    }
}

int AgenteAestrella::Step(int X, int Y){
	//aqui simplemente va recorriendo la lista con el 
	//camino ya generado y devolviendo la posicion a
	//la que se mueve el vehiculo, a no ser que llegue
	//al nodo final, en ese caso devuelve 4
	// 0=arriba,1=Izquierda,2=Derecha,3=abajo
    if(this->final_list != nullptr){
	    this->current = this->final_list->pop_last();
	    //si la lista está vacía signfica que no hay camino
	    if(this->current == nullptr){
		    return 4;
	    }
	    //arriba
	    if((Y - this->current->getY()) == 1){
		    return 0;
	    //Izquierda
	    }else if((X - this->current->getX())==1){
		    return 1;
	    //derecha
	    }else if((this->current->getX() - X)==1){
		    return 2;
	    //abajo
	    }else if((this->current->getY() - Y)==1){
		    return 3;
	    }
    } else {
        return 4;
    }
}

int AgenteAestrella::H(int xorigen,int yorigen, int xdestino, int ydestino){
	return abs(xdestino-xorigen) + abs(ydestino-yorigen);
}

int AgenteAestrella::H(int xorigen,int yorigen, int xdestino, int ydestino, int tipoHeuristica){
	if(tipoHeuristica==2) {
        return sqrt(pow(xdestino-xorigen,2)+pow(ydestino-yorigen,2)); //Raíz cuadrada de la suma de cuadrados
	}
	else{
	    return abs(xdestino-xorigen) + abs(ydestino-yorigen); //Suma de los valores absolutos
	}
}

int AgenteAestrella::get_cantidad_celdas(){
    return this->cantidad_celdas;
}
