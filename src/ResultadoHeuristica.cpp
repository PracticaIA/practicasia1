#include "ResultadoHeuristica.h"

/*
 * Primera forma de generar un resultado:
 * Se crea el constructor siguiente y luego se hace un addHeuristica
 */
ResultadoHeuristica::ResultadoHeuristica(int M, int N, int porcentObsta) {
    this->M = M;
    this->N = N;
    this->porcentObsta = porcentObsta;
}

/*
 * Segunda forma de generar el resultado:
 * Se crea el constructor pasando al final un vector con las heurísticas
 */
ResultadoHeuristica::ResultadoHeuristica(int M, int N, int porcentObsta, vector<int> longitudCamino, vector<int> CPU, vector<int> nodosGenerados)
{
    this->M = M;
    this->N = N;
    this->porcentObsta = porcentObsta;
    this->longitudCamino = longitudCamino;
    this->CPU = CPU;
    this->nodosGenerados = nodosGenerados;
}

ResultadoHeuristica::~ResultadoHeuristica() {}

int ResultadoHeuristica::getM() {
    return(this->M);
}

int ResultadoHeuristica::getN() {
    return(this->N);
}

int ResultadoHeuristica::getPorcentObsta() {
    return(this->porcentObsta);
}

vector<int> ResultadoHeuristica::getLongitudCamino() {
    return(this->longitudCamino);
}

vector<int> ResultadoHeuristica::getCPU() {
    return(this->CPU);
}

vector<int> ResultadoHeuristica::getNodosGenerados() {
    return(this->nodosGenerados);
}

void ResultadoHeuristica::mostrar() {
    cout << this->M << "X" << this->N << "\t\t" << this->porcentObsta << "%\t\t";
    for(int i=0; i<longitudCamino.size(); i++) {
        cout << longitudCamino[i] << "\t\t";
        cout << CPU[i] << "\t";
        cout << nodosGenerados[i] << "\t\t";
    }
    cout << endl;
}

void ResultadoHeuristica::addHeuristica(int longitudCamino, int CPU, int nodosGenerados) {
    this->longitudCamino.push_back(longitudCamino);
    this->CPU.push_back(CPU);
    this->nodosGenerados.push_back(nodosGenerados);
}
