#include "linked_list.h"

linked_list::linked_list(){
	this->Head = nullptr;
	this->Tail = nullptr;
	this->temp = nullptr;
	this->size = 0;
}

void linked_list::append(Celda* cell){
	//si es nullptr(es decir, la lista esta vacia
	if(this->Head == nullptr){
		//head apunta al elemento creado, igual que tail, size vale 1
		this->Head = new Node(cell);
		this->Tail = this->Head;
		size = 1;
	} else {
		//si la lista tiene algun elemento
		//creamos un nodo nuevo, cuyo previous apunte al último elemento de la lista
		//y hacemos que tail apunte al nuevo elemento, incrementamos el tamaño
		this->temp = new Node(cell,this->Tail,nullptr);
		this->Tail->set_next(this->temp);
        this->temp->set_previous(this->Tail);
		this->Tail = this->temp;
		size++;
	}
}

void linked_list::append(Node* node){
    Node * mynode;
    mynode = new Node(node);
    if(this->Head == nullptr){
        this->Head = mynode;
        this->Tail = this->Head;
        size = 1;
    } else {
        this->Tail->set_next(mynode);
        mynode->set_previous(this->Tail);
        mynode->set_next(nullptr);
        this->Tail = mynode;
        size++;
    }
}

void linked_list::remove_last(){
	//si hay algun elemento en la lista
	if(this->Head != nullptr){
		//obtenemos el elemento anterior
		this->temp = this->Tail->get_previous();
		//si el elemento anterior no es nullptr (es decir, hay mas de un elemento)
		if (this->temp != nullptr){
			//lo eliminamos, apuntamos tail al ahora ultimo elemento y decrementamos el tamaño
			delete this->Tail;
			this->Tail = this->temp;
			size--;
		} else {
			//si el elemento anterior es nullptr(es decir, este era el ultimo elemento de la lista
			//establecemos el tamaño a 0, eliminamos el ultimo elemento y establecemos 
			// tail y head a nullptr
			size = 0;
			delete this->Tail;
			this->Head = nullptr;
			this->Tail = nullptr;
		}
	}

}

///elimina el nodo especificado de la lista y devuelve su puntero
Node* linked_list::remove_at(int i){
    this->temp = this->get_at(i);
    if(this->Head != nullptr){
        if(this->temp == this->Head){
            if(this->Head->get_next() == nullptr){
                this->Head = nullptr;
                this->Tail = nullptr;
            } else {
                this->Head = this->Head->get_next();
                this->Head->set_previous(nullptr);
            }
        } else if(this->temp == this->Tail){
            if(this->Tail->get_previous() == nullptr){
                this->Head = nullptr;
                this->Tail = nullptr;
            } else {
                this->Tail = this->Tail->get_previous();
            }
        } else {
            //hacemos que el posterior anpunte al anterior
            this->temp->get_next()->set_previous(this->temp->get_previous());

            //hacemos que el anterior apunte al posterior
            this->temp->get_previous()->set_next(this->temp->get_next());
        }
        size--;
    }
    return this->temp;
}

//elimina el primer nodo de la lista y lo elimina de la memoria
void linked_list::remove_first(){
    if(this->Head != nullptr){
        this->temp = this->Head->get_next();
        if(this->temp != nullptr){
            delete this->Head;
	        this->Head = this->temp;
	        this->Head->set_previous(nullptr);
        } else {
	        delete this->Head;
        }
        size--;
    }
}

void linked_list::remove_node(Node* node){
    if(node != nullptr){
        Node* mynode = new Node(node);
        this->temp = this->Head;
        while(this->temp != nullptr){
            if(mynode->isEqual(this->temp)){
                if(this->temp->get_next() != nullptr){
                    this->temp->get_next()->set_previous(this->temp->get_previous());
                }
                if(this->temp->get_previous() != nullptr){
                    this->temp->get_previous()->set_next(this->temp->get_next());
                }
                size--;
                break;
            } else {
                this->temp = this->temp->get_next();
            }   
        } 
    }

}

//elimina el primer nodo de la lista y devuelve un puntero a la celda que contenia
Celda* linked_list::pop_first(){
    Celda* temp;
    if(this->Head != nullptr){
    	temp = this->Head->get_celda();
    	this->remove_first();
    	return temp;
    } else {
	    return nullptr;
    }
}

Celda* linked_list::pop_last(){
    Celda* temp;
    if(this->Head !=nullptr){
        temp = this->Tail->get_celda();
        this->remove_last();
        return temp;
    } else {
        return nullptr;
    }
}

//devuelve la ultima celda de la lista, sin eliminarla
Celda* linked_list::get_last(){
	if (this->Tail != nullptr){
		return this->Tail->get_celda();
	}else{
		return nullptr;
	}
}

//devuelve el ultimo nodo de la lista, sin eliminarlo
Node* linked_list::get_last_node(){
	if (this->Tail != nullptr){
		return this->Tail;
	} else {
		return nullptr;
	}
}

//devuelve el primer nodo de la lista sin elimnarlo
Node* linked_list::get_first_node(){
    if (this->Head != nullptr){
        return this->Head;
    } else {
        return nullptr;
    }
}

//return the first node, remove it from the list but not from the memory
Node* linked_list::pop_first_node(){
    if(this->Head != nullptr){
        this->temp = this->Head;
        this->Head = this->Head->get_next();
        if(this->Head != nullptr){
            this->Head->set_previous(nullptr);
        } else {
            this->Tail = nullptr;
        }
        size--;
        return this->temp;
    }
    return nullptr;
}

//comprueba si la celda está en la lista, en caso afirmativo devuelve el nodo que la contiene, en caso negativo devuelve nullptr
Node* linked_list::in_path(Celda* cell){
	this->temp = this->Head;
	while(this->temp != nullptr){
		if(this->temp->get_celda() == cell){
			return this->temp;
		}
		this->temp = this->temp->get_next();
	}
	return nullptr;
}

//devuelve el tamanio de la lista
int linked_list::get_size(){
	return this->size;
}

//devuelve el nodo en una determinada posicion
Node* linked_list::get_at(int at){
    this->temp = this->Head;
    for(int i=0;i<at;i++){
        this->temp = this->temp->get_next();
    }
    return this->temp;
}

//inserta un nodo
void linked_list::insert(int pos, Node* node){
    //obtenemos el nodo en dicha posicion
    this->temp = this->get_at(pos);
    //establecemos en node, cual será su predecesor y sucesor
    node->set_previous(this->temp->get_previous());
    node->set_next(this->temp);
    //actualizamos los punteros de los nodos alrededor afectados
    if(this->temp->get_previous() != nullptr){
        this->temp->get_previous()->set_next(node);
    }else if(this->size == 0){
        this->Head = node;
        this->Tail = node;
    } else {
        this->Head = node;
    }
    this->temp->set_previous(node);
    size++;
}

//ordena la lista
void linked_list::sort(){
    //esto no es optimo pero es valido 
    int actual_value = -1;
    Node* unsortedptr;
    for(int i=0;i<this->size;i++){
        unsortedptr = this->get_at(i);
        for(int j=0; j<i;j++){
            if(unsortedptr->get_f()  < this->get_at(j)->get_f()){
                //eliminamos el nodo insertado de su antigua posicion
                this->remove_at(i);
                //insertamos
                this->insert(j,unsortedptr);
                break;
            }
        }
    }
}
