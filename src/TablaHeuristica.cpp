#include "TablaHeuristica.h"

TablaHeuristica::TablaHeuristica() {}

TablaHeuristica::TablaHeuristica(vector<ResultadoHeuristica> resultados) {
    this->lineasResultados = resultados;
}

TablaHeuristica::~TablaHeuristica() {}

void TablaHeuristica::addResultado(ResultadoHeuristica dato) {
    this->lineasResultados.push_back(dato);
}

void TablaHeuristica::mostrar() {
    //Cabecera
    std::cout << "Dimensión" << "\t";
    std::cout << "Obstáculos" << "\t";
    std::cout << "Longitud" << "\t";
    std::cout << "CPU" << "\t";
    std::cout << "Nodos" << "\t";
    std::cout << std::endl;
    //Líneas de resultados
    for(int i=0; i<this->lineasResultados.size(); i++) {
        this->lineasResultados[i].mostrar();
    }
}

