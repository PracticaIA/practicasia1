#include "Node.h"

Node::Node(){
	this->previous_node = nullptr;
	this->next_node = nullptr;
	this->current = nullptr;
    this->parent = nullptr;
    this->g = 0;
    this->h = 0;
	for(int i=0;i<4;i++){
		this->visited_nodes[i] = false;
	}
}

Node::Node(Node* node){
    this->previous_node = node->get_previous();
    this->next_node = node->get_next();
    this->current = node->get_celda();
    this->parent = node->get_parent();
    this->g = node->get_g();
    this->h = node->get_h();
}

Node::~Node(){
	
}

Node::Node(Celda* cell, Node* previous, Node* next){
	this->current = cell;
	this->previous_node = previous;
	this->next_node = next;
    this->h = 0;
    this->g = 0;
    this->parent = nullptr;
}

Node::Node(Celda* cell){
	this->current = cell;
	this->previous_node = nullptr;
	this->next_node = nullptr;
    this->h = 0;
    this->g = 0;
}

void Node::set_visited(int node, bool status){
	this->visited_nodes[node] = true;
}

bool Node::get_visited(int node){
	return this->visited_nodes[node];
}

Celda* Node::get_celda(){
	return this->current;
}

void Node::set_celda(Celda* celda){
	this->current = celda;
}

Node* Node::get_previous(){
	return this->previous_node;
}

void Node::set_previous(Node* node){
	this->previous_node = node;
}

Node* Node::get_next(){
	return this->next_node;
}

void Node::set_next(Node* node){
	this->next_node = node;
}

int Node::get_g(){
    return this->g;
}

int Node::get_h(){
    return this->h;
}

int Node::get_f(){
    return (this->h + this->g);
}

void Node::set_g(int g){
    this->g = g;
}

void Node::set_h(int h){
    this->h = h;
}

void Node::set_parent(Node* node){
    this->parent = node;
}

Node* Node::get_parent(){
    return this->parent;
}

bool Node::isEqual(Node* other){
    if(this->get_celda() == other->get_celda()){
        if(this->get_g() == other->get_g()){
            if(this->get_h() == other->get_h()){
                return true;
            }
        }
    }
    return false;

}
