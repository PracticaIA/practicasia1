#include <vector>

#include "Matriz.h"

using namespace std;

//constructor con parámetros
///M -> Ancho
///N -> Alto 
Matriz::Matriz(int M, int N, vector<Vehiculo*> VehicVector, bool Aleat, int porcentObsta){
	//asigno valores y inicializamos la semilla
	srand (time(NULL));
	this->StepCounter = 0;
	this->M = M;
	this->N = N;
	this->VehicVector = VehicVector;

	//asigno valores a los punteros de vehiculo
	for(int i=0;i<this->VehicVector.size();i++){
		this->VehicVector[i]->SetParent(this);
	}

	//creo las celdas y guardo sus punteros en matrix
	this->matrix.resize(this->N);
	for (int i=0;i<this->N;i++){
		this->matrix[i].resize(this->M);
		for (int j=0;j<this->M;j++){
			matrix[i][j] = new Celda(i,j);
			if (Aleat) {
				//matrix[i][j]->setObstaculo(rand()%2); //entre 0 y 1
		                bool TrueFalse = (rand() % 100) < porcentObsta;
               			matrix[i][j]->setObstaculo(TrueFalse);
				if (rand()%2){
					matrix[i][j]->setPasajeros(int(rand()%10));//entre 0 y 10
				}
			}
		}
	}
	//elijo una celda al azar como destino
	if(Aleat) {
            //chapuza, pero es tarde
            bool failed = true;
        while(failed){
		    int x = (rand() % this->GetWidth());
		    int y = (rand() % this->GetHeight());
            if(!this->GetCelda(x,y)->isObstaculo()){
		        this->setMeta(x,y);
                failed = false;
            } else {
                failed = true;
            }
        }
	}
}

void Matriz::Resolve(){
       //llamamos al resolve del vehiculo (chapuza, lo ideal sería llamar a un resolve de la matriz)
        VehicVector[0]->Resolve();

}

void Matriz::Print(){
	//declaramos variables necesarias
	bool printed = false;
	//limpiamos la pantalla
	this->ClearScreen();
	//imprimimos el Step Counter
	cout << "Step Counter: " << this->StepCounter << endl;
	//recorremos la matriz
	for (int i=0;i<this->N;i++){
		//nueva línea, la empezamos con una |
		cout << "|";
		for (int j=0;j<this->M;j++){
			//comprobamos si en este sitio hay algún vehículo
			for(int k=0; k<this->VehicVector.size();k++){
				//si hay vehículo lo pintamos y dejamos de buscar vehículos
				if(this->VehicVector[k]->GetX() == j && this->VehicVector[k]->GetY() == i){
					this->VehicVector[k]->Print();
					cout << "|";
					printed = true;
					break;
				}
			}
			//si no hemos pintado el vehiculo
			if (!printed){
				this->matrix[j][i]->Print(); 
				cout << "|";
			//si lo hemos pintado, acabamos
			} else {
				printed = false;
			}
		}
		cout << endl;
	}
}

void Matriz::ClearScreen(){
	for (int i=0; i<100;i++){
		cout << endl;
	}
}

int Matriz::Step(){
	this->StepCounter++;
	//en realidad este bucle va a tener siempre una sola iteración
	for (int i =0; i<this->VehicVector.size();i++){
		//devuelvo el valor que ha devuelto el step(2=sin camino, 2=sin camino, 1=final, 0=en camino)
		return this->VehicVector[i]->Step();
	}
	return 0;
}

Celda* Matriz::GetCelda(int X, int Y){
	if(X>= this->GetWidth() || X<0 || Y >= this->GetHeight() || Y<0){
		return nullptr;
	}else{
		return this->matrix[X][Y];
	}
}

int Matriz::GetWidth(){
	return this->N;
}

Vehiculo* Matriz::GetVehiculo(int N){
	if(N < this->VehicVector.size());
		return this->VehicVector[N];
}

int Matriz::GetHeight(){
	return this->M;
}

Celda* Matriz::GetMeta(){
	return this->meta;
}

void Matriz::setMeta(int X, int Y){
	this->meta = this->GetCelda(X,Y);
	this->meta->setMeta(true);
}

int Matriz::get_cantidad_nodos(){
    return this->VehicVector[0]->get_cantidad_nodos();
}
