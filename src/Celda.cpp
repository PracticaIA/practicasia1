#include "Celda.h"

/**
 * Constructor por defecto de la clase Celda.
 * Añade por defecto a la celda: 0 pasajeros, peso 1 y sin obstáculo
 */
Celda::Celda() {
    this->pasajeros = 0;
    this->peso = 1;
    this->obstaculo = false;
    this->meta = false;
    strcpy(this->color,"");
}

Celda::Celda(int X, int Y){
    this->pasajeros = 0;
    this->peso = 1;
    this->obstaculo = false;
    this->meta = false;
    strcpy(this->color,"");
    this->X = X;
    this->Y = Y;
}

/**
 * Destructor clase Celda
 */
Celda::~Celda() {}

int Celda::getPeso() const {
    return this->peso;
}

int Celda::getPasajeros() const {
    return this->pasajeros;
}

bool Celda::isObstaculo() const {
    return this->obstaculo;
}

bool Celda::isMeta() const {
    return this->meta;
}

void Celda::setPeso(int peso) {
    this->peso = peso;
}

void Celda::setPasajeros(int pasajeros) {
    this->pasajeros = pasajeros;
}

void Celda::setObstaculo(bool obstaculo) {
    this->obstaculo = obstaculo;
}

void Celda::setMeta(bool meta) {
    this->meta = meta;
}

void Celda::setTraveled(int color){
    if(color == 0){
	strcpy(this->color,"");
    }else if (color == 1){
	strcpy(this->color,"\033[1;31m");
    }
}

void Celda::Print(){
    if(this->obstaculo){
	cout << "X";
    } else if (this->meta) {
	cout << "\033[1;31mM\033[0m";
    } else {
	if (!strcmp(this->color,"")){
	    cout << " ";
	}else{
	    cout << this->color << char(254) << "\033[0m";
	}
    }
}

void Celda::setX(int X){
    this->X = X;
}

void Celda::setY(int Y){
    this->Y = Y;
}

int Celda::getX(){
    return this->X;
}

int Celda::getY(){
    return this->Y;
}
