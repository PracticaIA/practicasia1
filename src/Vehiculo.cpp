#include "Vehiculo.h"

using namespace std;

Vehiculo::Vehiculo(Matriz *Parent, Agente *Agent, int X, int Y){
	this->Parent = Parent;
	this->Agent = Agent;
	this->X = X;
	this->Y = Y;
	this->meta = false;
}

void Vehiculo::Resolve(){
	//calculo el camino usando el agente
	Agent->Resolve(this->Parent,this->X,this->Y);
}

void Vehiculo::Print(){
    	cout << "\033[1;31mT\033[0m";
}

int Vehiculo::Step(){
	///genero variables locales
	int nextpos;
	///ejecuto el step para mover el vehículo a donde diga el agente
	nextpos = Agent->Step(this->X,this->Y);
	//establezco la celda como visitada para dibujar el camino antes de mover el vehículo
        this->Parent->GetCelda(this->X, this->Y)->setTraveled(1);
	///muevo el vehiculo (siempre que se pueda)
	switch(nextpos){
		///Arriba
		case 0:
			if (!this->Y-1 <0){
				this->Y--;
				this->CheckCrash();
				this->CheckFinal();
			}
			break;
		///Izquierda
		case 1:
			if(!(this->X-1 <0)){
				this->X--;
				this->CheckCrash();
				this->CheckFinal();
			}
			break;
		///Derecha
		case 2:
			if(!(this->X+1 >= this->Parent->GetWidth())){
				this->X++;
				this->CheckCrash();
				this->CheckFinal();
			}
			break;
		///Abajo
		case 3: 
			if(!(this->Y+1 >= this->Parent->GetHeight())){
				this->Y++;
				this->CheckCrash();
				this->CheckFinal();
			}
			break;
		case 4:
			return 2;
			break;
	}
	//si no es el nodo final, devuelvo 0
	if(this->meta){
		return 1;
	} else {
		return 0;
	}
}

int Vehiculo::GetX(){
	return this->X;
}

int Vehiculo::GetY(){
	return this->Y;
}

void Vehiculo::SetPosition(int X, int Y){
	this->X = X;
	this->Y = Y;
}

void Vehiculo::SetParent(Matriz *Parent){
	this->Parent = Parent;
}

void Vehiculo::CheckCrash(){
	if(Parent->GetCelda(this->X,this->Y)->isObstaculo()){
		throw "El Agente ha estrellado el vehiculo";
	}
}

void Vehiculo::CheckFinal(){
	if(Parent->GetCelda(this->X,this->Y)->isMeta()){
		this->meta = true;
	}
}

int Vehiculo::get_cantidad_nodos(){
    return this->Agent->get_cantidad_celdas();
}
