#include "Matriz.h"
#include "Vehiculo.h"
#include "AgenteAestrella.h"
#include "TablaHeuristica.h"

#include <vector>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;

//variables globales
Matriz *matrix;
int velocidad = 1000;

void initialize(int ancho, int alto, int NumVehic, bool aleatorio, int porcentObsta, bool tipo_agente){
        AgenteAestrella *Agenteptr = new AgenteAestrella(tipo_agente);
        vector<Vehiculo*> VehicVector;

        VehicVector.resize(NumVehic);
        for (int i=0;i<NumVehic;i++){
                VehicVector[i] = new Vehiculo(nullptr,Agenteptr,0,0);
        }
        matrix = new Matriz(alto, ancho, VehicVector, aleatorio, porcentObsta);
}
void saveFile(Matriz* matrix, string nombre){
    ofstream archivo;
    Vehiculo *vehiculo;
    string ruta = "maps/";
    archivo.open(ruta.append(nombre), ios::out);
    int ancho,alto,aleatorio,porcentajealeatorio,numvehi,velsimulacion;
    ancho=matrix->GetWidth();
    alto=matrix->GetHeight();
    porcentajealeatorio=0;
    numvehi=1;
    velsimulacion=0;
    vehiculo = matrix->GetVehiculo(0);

    archivo << ancho << "," <<  alto << "," << 1 << "," << porcentajealeatorio << "," << numvehi << "," << 0 << endl;
    // X,Y,tipo(1=obstaculo,2=pasajeros,3=vehiculos),cantidadpersonas
    for(int i=0;i<ancho;i++){
        for(int j=0;j<alto;j++){
            if (matrix->GetCelda(i,j)->isMeta()){
                archivo << i << "," << j << ",4,0\n";
            }else if(matrix->GetCelda(i,j)->isObstaculo()){
                archivo << i << "," << j << ",1,0\n";
            }else if(matrix->GetCelda(i,j)->getPasajeros() > 0){
                archivo << i << "," << j << ",2," << matrix->GetCelda(i,j)->getPasajeros() << "\n";
            }else if((vehiculo->GetX() == i) && (vehiculo->GetY() == j)){
                archivo << i << "," << j << ",3,0\n";
            }
        }
    }
    archivo.close();
}


void LoadFile(int &ancho, int &alto, bool &aleatorio, int &porcentObsta, int &NumVehic, string filename, bool tipo_agente){
	string line,header_item;
	vector<Vehiculo*> VehicVector;
	string data[6];
	int pos,iter,x,y,tipo,cantidad;
	ifstream config(filename);
	string delimiter = ",";
	if (config.is_open()){
		//read header
		// ancho,alto,aleatorio,porcentajealeatorio,numerovehiculos,velocidadsimulacion(ms)
		getline(config,line);
		iter = 0;
		for(int i=0; i<6;i++){
			if (pos = line.find(delimiter) == std::string::npos){
				pos = line.length();
			}
			pos = line.find(delimiter);
			header_item = line.substr(0,pos);
			data[i] = header_item;
			line.erase(0,pos+delimiter.length());
		}
		//guardo las cabeceras
		ancho = stoi(data[0]);
		alto = stoi(data[1]);
		aleatorio = bool(stoi(data[2]));
		porcentObsta = stoi(data[3]);
		NumVehic = stoi(data[4]);
		velocidad = stoi(data[5]);
		//creo la matriz
		initialize(ancho,alto,NumVehic,aleatorio,porcentObsta,tipo_agente);
		//creo los obstáculos o pasajeros definidos
		// X,Y,tipo(1=obstaculo,2=pasajeros,3=vehiculos,4=meta),cantidad
		while(getline(config,line)){
			pos = line.find(delimiter);
			x = stoi(line.substr(0,pos));
			line.erase(0,pos+1);
			pos = line.find(delimiter);
			y = stoi(line.substr(0,pos));
			line.erase(0,pos+1);
			pos = line.find(delimiter);
			tipo = stoi(line.substr(0,pos));
			line.erase(0,pos+1);
			cantidad = stoi(line.substr(0,line.length()));
			if(tipo == 1){
				matrix->GetCelda(x,y)->setObstaculo(true);
			} else if(tipo == 2){
				matrix->GetCelda(x,y)->setPasajeros(cantidad);
			} else if(tipo == 3){
				matrix->GetVehiculo(cantidad)->SetPosition(x,y);
			} else if(tipo == 4){
				matrix->setMeta(x,y);
			}
		}
	}
}

void askDataToUser(){
	int Ancho,Alto,NumVehic,numObstaculos, porcentObsta;
	bool aleatorio = false;
	string str;
	vector<Vehiculo*> VehicVector;
	cout << "¿Desea cargar desde fichero?[S/N]:";
	cin >> str;
	if (str == "S" || str == "s"){
		LoadFile(Alto,Ancho,aleatorio,porcentObsta,NumVehic,"config.ini",0);
	} else {
		cout << "Introduzca el ancho del tablero:";
		cin >> Ancho;
		cout << endl << "Introduzca el alto del tablero:";
		cin >> Alto;
		cout << endl << "Introduzca la velocidad de la simulación (ms):";
		cin >> velocidad;
		cout << endl << "Introduzca el numero de vehiculos:";
		cin >> NumVehic;
		cout << endl << "Obstáculos en modo aleatorio:" << endl << "1.Predefinido" << endl << "2.Porcentaje(0-100)"<< endl;
		cin >> numObstaculos;
		switch(numObstaculos) {
			case 1:
            			porcentObsta = 20;
            			aleatorio = true;
				break;
    		    	case 2:
            			aleatorio = true;
            			cout << "Porcentaje obstáculos: ";
            			cin >> porcentObsta;
            			while (porcentObsta < 0 || porcentObsta > 100)
            			{
                			cout << "Dato incorrecto. Introducir(0-100): ";
                			cin >> porcentObsta;
            			}
            			break;
			default:
            			cout << "Opción no reconocida." << endl;
				break;
		}
		initialize(Ancho,Alto,NumVehic,aleatorio,porcentObsta,0);
	}
}

void printHelp(char * arg){
	cerr << "Command "<< arg << " not recognized\n";
	cerr << "Command list:\n";
	cerr << "--no-interactive\n";
    cerr << "--generate-maps\n";
    cerr << "--generate-table\n";
}

int main(int argc, char* argv[])
{
	int returnvalue;
	if (argc < 2){
		//pregunto al ususario e inicializo
		askDataToUser();
		matrix->Print();
		matrix->Resolve();
		while(true){
			matrix->Print();
			try {
				returnvalue = matrix->Step();
				if(returnvalue == 1){
					usleep(velocidad*1000); //microseconds
					matrix->Print();
					break;
				} else if(returnvalue == 2){
					cerr << "No se ha encontrado ningún camino posible" << endl;
					break;
				}
			} catch (const char* msg){
				matrix->Print();
				cerr << msg << endl;
				return 5;
			}
			usleep(velocidad*1000); //microseconds
		}
	}else if (!strcmp(argv[1],"--no-interactive")){
		//inicializo con valores preestablecidos
		initialize(20,20,1,1,5,0);
		matrix->Resolve();
		for (int i =0;i<1000;i++){
			//matrix->Print();
			try {
				if(matrix->Step() == 1){
					matrix->Print();
					break;
				} else if(returnvalue == 2){
					cerr << "No se ha encontrado ningún camino posible" << endl;
					break;
				}
			} catch(const char* msg){
				matrix->Print();
				cerr << msg << endl;
				return 5;
			}
		}
		return 0;
	 }else if (!strcmp(argv[1],"--generate-maps")){
        //declaramos una string que contenga el nombre del fichero
        string name;
        //bucle que genera los mapas aleatorios
        for (int i=10;i<=100;i=i+10){ //ancho
            for(int j=10;j<=100;j=j+10){ //alto
                for(int k=0;k<=90;k=k+10){ //porcentaje obstaculos
                    initialize(i,j,1,1,k,0);
                    name.append(to_string(i));
                    name.append("x");
                    name.append(to_string(j));
                    name.append("-");
                    name.append(to_string(k));
                    name.append(".ini");
                    saveFile(matrix,name);
                    delete matrix;
                    name.clear();
                }
            }
        }
     }else if(!strcmp(argv[1],"--generate-table")){
        TablaHeuristica Tabla;
        string path = "maps/";
        string name;
        int ancho,alto,porcentObsta,NumVehic;
        bool aleatorio;
        int longitud =0;
        //bucle que genera los nombre de los mapas aleatorios (usar libs externas en diferentes so's complica bastante y es tarde
        for(int i=10;i<=100;i=i+20){
            for(int k=0;k<=80;k=k+20){
                for(int l=0;l<2;l++){
                    longitud=0;
                    //generamos el objeto de resultados y lo rellenamos
                    ResultadoHeuristica result(i,i,k);
                    name.append(to_string(i));
                    name.append("x");
                    name.append(to_string(i));
                    name.append("-");
                    name.append(to_string(k));
                    name.append(".ini");
                    //cargamos el fichero
                    LoadFile(ancho,alto,aleatorio,porcentObsta,NumVehic,path.append(name),l);
                    //iteramos
                    matrix->Resolve();
                    while(true){
                        try {
                            returnvalue = matrix->Step();
                            if(returnvalue == 1){ 
                                break;
                            } else if(returnvalue == 2){ 
                                //cerr << "No se ha encontrado ningún camino posible" << endl;
                                break;
                            }
                        } catch (const char* msg){
                            cerr << msg << endl;
                            return 5;
                        }
                        longitud++;
                    }
                    //terminamos de rellenar los datos y los adjuntamos a la tabla
                    result.addHeuristica(longitud, l, matrix->get_cantidad_nodos());
                    Tabla.addResultado(result);
                    //result.mostrar();
                    delete matrix;
                    path = "maps/";
                    name.clear();
                }
            }
        }       
        Tabla.mostrar();
     }else{
		printHelp(argv[1]);
		return 2;
	}
}
