![demo](gif.gif)

# Como organizarnos

1. no se hacen push a master
2. un issue por cada funcion a implementar (del issue se saca un merge request y se implementa ahí)

# Comandos
Descargar cambios

``` git pull ```

Subir cambios

```git push```

Añadir cambios locales

```git add .```

Hacer commit

```git commit -m"MENSAJE"```

Ver estado

``` git status```

Cambiar de rama

``` git checkout "nombre de rama" ```

Merge y demás se hace a través de la interfaz web
