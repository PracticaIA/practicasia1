#ifndef AGENTETONTO_H
#define AGENTETONTO_H

#include "Agente.h"

class AgenteTonto : public Agente{
public:
	AgenteTonto();
	void Resolve(Matriz* matrix);
	int Step();
};

#endif
