#ifndef AGENTE_H
#define AGENTE_H

#include "Celda.h"

class Matriz;

class Agente
{
public:
	Agente() {}
	virtual void Resolve(Matriz* matrix,int X, int Y)=0;
	virtual int Step(int X, int Y)=0;
    virtual int get_cantidad_celdas()=0;
private:
};

#endif
