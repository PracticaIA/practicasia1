#ifndef MATRIZ_H
#define MATRIZ_H

#include "Celda.h"
#include "Vehiculo.h"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

class Vehiculo;

class Matriz
{
public:
	Matriz(int N, int M, vector<Vehiculo*> VehicVector, bool Aleat=false, int porcentObsta = 20);
	void Print();
	int Step();
	void Resolve();
	Celda* GetCelda(int X, int Y);
	Celda* GetMeta();
	void setMeta(int X, int Y);
	Vehiculo* GetVehiculo(int N);
	int GetWidth();
	int GetHeight();
    int get_cantidad_nodos();
private:
	vector<vector<Celda*> > matrix;
	vector<Vehiculo*> VehicVector;
	Celda* meta;
	int M;
	int N;
	int StepCounter;
	void ClearScreen();
};
#endif
