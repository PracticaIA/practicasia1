#ifndef TABLAHEURISTICA_H
#define TABLAHEURISTICA_H

#include <vector>
#include "ResultadoHeuristica.h"

class TablaHeuristica {

    private:
        vector<ResultadoHeuristica> lineasResultados;

    public:
        TablaHeuristica();
        TablaHeuristica(vector<ResultadoHeuristica>);
        virtual ~TablaHeuristica();

        void addResultado(ResultadoHeuristica);
        void mostrar();

};

#endif

