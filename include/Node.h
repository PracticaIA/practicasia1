#ifndef NODE_H
#define NODE_H

#include "Celda.h"

class Node{
public:
	Node();
    Node(Node* node);
	~Node();
	Node(Celda* cell);
	Node(Celda* cell, Node* previous, Node* next);
	Node* get_previous();
	void set_previous(Node* node);
	Node* get_next();
	void set_next(Node* node);
	Celda* get_celda();
	void set_celda(Celda* celda);
	void set_visited(int node, bool status);
	bool get_visited(int node);
    int get_g();
    int get_h();
    int get_f();
    void set_g(int g);
    void set_h(int h);
    void set_parent(Node* node);
    Node* get_parent();
    bool isEqual(Node* node);

private:
	Node* previous_node;
	Node* next_node;
    Node* parent;
	Celda* current;
    int g;
    int h;
	bool visited_nodes[4];
};

#endif
