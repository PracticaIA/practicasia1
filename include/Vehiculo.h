#ifndef VEHICULO_H
#define VEHICULO_H
#include <stdio.h>

#include "Agente.h"
#include "Matriz.h"
#include "Celda.h"

class Matriz;

class Vehiculo
{
public:
	Vehiculo(Matriz *Parent, Agente *Agent, int X, int Y);
	void SetParent(Matriz *Parent);
	int Step();
	int GetX();
	void SetPosition(int X, int Y);
	void Resolve();
	int GetY();
	void Print();
    int get_cantidad_nodos();
private:

	void CheckCrash();
	void CheckFinal();
	int X, Y;
	bool meta;
	Agente *Agent;
	Matriz *Parent;
	Celda *TempCelda[4];
};
#endif
