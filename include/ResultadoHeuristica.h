#ifndef RESULTADOHEURISTICA_H
#define RESULTADOHEURISTICA_H

#include <vector>
#include <iostream>
using namespace std;

/* DATOS:
 * Dimensiones (20X20)
 * Obstáculos 10%
 * Heurística 1
 * * Longitud del Camino
 * * CPU
 * * Nodos Generados
 * Heurística 2
 * * Longitud del Camino
 * * CPU
 * * Nodos Generados
 * ...
 */

class ResultadoHeuristica {

    private:
        int M;
        int N;
        int porcentObsta;
        vector<int> longitudCamino;
        vector<int> CPU;
        vector<int> nodosGenerados;

    public:
        ResultadoHeuristica(int M, int N, int porcentObsta);
        ResultadoHeuristica(int M, int N, int porcentObsta, vector<int> longitudCamino, vector<int> CPU, vector<int> nodosGenerados);
        virtual ~ResultadoHeuristica();

        int getM();
        int getN();
        int getPorcentObsta();
        vector<int> getLongitudCamino();
        vector<int> getCPU();
        vector<int> getNodosGenerados();

        void addHeuristica(int longitudCamino, int CPU, int nodosGenerados);
        void mostrar();

};


#endif
