#ifndef AGENTEAESTRELLA_H
#define AGENTEAESTRELLA_H

#include "Agente.h"
#include "Matriz.h"
#include "linked_list.h"

class AgenteAestrella : public Agente{
public:
	AgenteAestrella();
    AgenteAestrella(bool tipo_agente);
	void Resolve(Matriz *matrix, int X, int Y);
	int Step(int X, int Y);
    int get_cantidad_celdas();
	int H(int xorigen,int yorigen, int xdestino, int ydestino);
	int H(int xorigen,int yorigen, int xdestino, int ydestino, int tipoHeuristica);
private:
	linked_list* open_list;
    linked_list* closed_list;
    linked_list* final_list;
	int cantidad_celdas;
    bool tipo_agente;
	Celda* current;
	Celda* meta;
	Celda* vecinos[4];
	int g;
    int sucessor_current_cost;
    Node* nodeptr;
    Node* parentnode;
    Node* tempnode;
};
#endif
