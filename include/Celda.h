#ifndef CELDA_H
#define CELDA_H

#include <iostream>
#include <cstring>

using namespace std;

/**
 * Clase Celda para regular el comportamiento de cada celda de forma individual.
 */
class Celda {

    private:
        int peso;
	int X;
	int Y;
	char color[15];
        int pasajeros;
        bool obstaculo;
        bool meta;

    public:
        Celda();
	Celda(int X, int Y);
        virtual ~Celda();
        int getPeso() const;
        int getPasajeros() const;
        bool isObstaculo() const;
        bool isMeta() const;
        void setPeso(int peso);
	void setTraveled(int color);
        void setPasajeros(int pasajeros);
        void setObstaculo(bool obstaculo);
        void setMeta(bool meta);
	void setX(int X);
	void setY(int Y);
	int getX();
	int getY();
	void Print();
};
#endif
