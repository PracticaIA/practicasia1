#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "Node.h"

class linked_list{
public:
	linked_list();
	void append(Celda* cell);
    void append(Node* node);
	void remove_last();
	void remove_first();
    void remove_node(Node* node);
	Node* remove_at(int i);
	Celda* get_last();
	Node* get_last_node();
    Node* get_first_node();
    Node* pop_first_node();
	Celda* pop_first();
    Celda* pop_last();
	Node* in_path(Celda* cell);
	int get_size();
    Node* get_at(int at);
    void sort();

private:
    void insert(int at, Node* node);

	Node* Head;
	Node* Tail;
	Node* temp;
	int size;
};

#endif
